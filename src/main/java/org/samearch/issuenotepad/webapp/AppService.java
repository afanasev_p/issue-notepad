package org.samearch.issuenotepad.webapp;

import org.samearch.issuenotepad.webapp.controller.RedirectController;
import org.samearch.issuenotepad.webapp.controller.TaskController;
import org.samearch.issuenotepad.webapp.controller.TaskListController;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static spark.Spark.*;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Основной класс приложения.
 *
 * Задача - сконфигурировать и запустить http-сервер (Apache Spark).
 */
@Named
public class AppService {
	public static void main(String... args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("META-INF/spring.xml");
		AppService appService = context.getBean(AppService.class);
		appService.run();
	}

	private final TaskListController taskListController;
	private final TaskController taskController;

	@Inject
	public AppService(
			TaskListController taskListController,
			TaskController taskController
	) {
		this.taskListController = taskListController;
		this.taskController = taskController;
	}

	public void run() {
		get("/", new RedirectController(Constant.ROUTE_TASKS));
		get(Constant.ROUTE_TASKS, taskListController::serveGetIssues);
		post(Constant.ROUTE_TASKS, taskListController::servePostIssues);
		get(Constant.ROUTE_TASK, taskController::serveGetIssue);
		delete(Constant.ROUTE_TASK, taskController::serveDeleteIssue);
	}
}
