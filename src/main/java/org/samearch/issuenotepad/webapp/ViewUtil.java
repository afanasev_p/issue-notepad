package org.samearch.issuenotepad.webapp;

import org.apache.velocity.app.VelocityEngine;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.template.velocity.VelocityTemplateEngine;

import java.util.Map;

public class ViewUtil {

	public static Object render(Request request, Map<String, Object> model, String templatePath) {
		model.put("request", request);
		return strictVelocityEngine().render(new ModelAndView(model, templatePath));
	}

	public static Object redirect(Response response, String location) {
		response.redirect(location);
		return "";
	}

	public static Object ok(Response response) {
		response.status(200);
		return "";
	}

	private static VelocityTemplateEngine strictVelocityEngine() {
		VelocityEngine engine = new VelocityEngine();
		engine.setProperty("runtime.reference.strict", true);
		engine.setProperty("resource.loader", "class");
		engine.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
		return new VelocityTemplateEngine(engine);
	}
}
