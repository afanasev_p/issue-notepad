package org.samearch.issuenotepad.webapp;

public class Constant {
	// Обрабатываемые URL-ы. В блоке инициализации приложения для них устанавливаются обработчики.
	public static String ROUTE_TASKS = "/tasks";
	public static String ROUTE_TASK = "/task/:taskKey";

	public static String PATH_TO_XSD = "/schema/webapp.data.schema.xsd";

	// Пути к используемым шаблонам
	private static final String BASIC_TEMPLATE_PATH = "/templates";
	public static String TEMPLATE_ISSUES = BASIC_TEMPLATE_PATH + "/tasks.vm";
	public static final String TEMPLATE_ISSUE = BASIC_TEMPLATE_PATH + "/task.vm";

	// Атрибуты в шаблоне страницы
	public static final String PAGE_ISSUE_DATA_FILE_FIELD = "issuesDataFile";
}
