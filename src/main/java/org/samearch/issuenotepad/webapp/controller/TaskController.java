package org.samearch.issuenotepad.webapp.controller;

import org.samearch.issuenotepad.api.TaskService;
import org.samearch.issuenotepad.webapp.Constant;
import org.samearch.issuenotepad.webapp.ViewUtil;
import spark.Request;
import spark.Response;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.HashMap;

@Named
public class TaskController {
	private final TaskService taskService;

	private final String REQUEST_PARAM_TASK_KEY = ":taskKey";

	@Inject
	public TaskController(TaskService taskService) {
		this.taskService = taskService;
	}

	public Object serveGetIssue(Request request, Response response) {
		HashMap<String, Object> templateContext = new HashMap<>();
		String requestedTaskKey = request.params(REQUEST_PARAM_TASK_KEY);
		templateContext.put("task", taskService.getTask(requestedTaskKey));
		return ViewUtil.render(request, templateContext, Constant.TEMPLATE_ISSUE);
	}

	public Object serveDeleteIssue(Request request, Response response) {
		String requestedTaskKey = request.params(REQUEST_PARAM_TASK_KEY);
		taskService.deleteTask(requestedTaskKey);
		return ViewUtil.ok(response);
	}
}
