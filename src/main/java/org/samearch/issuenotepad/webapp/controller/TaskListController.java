package org.samearch.issuenotepad.webapp.controller;

import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import org.samearch.issuenotepad.api.Task;
import org.samearch.issuenotepad.api.TaskService;
import org.samearch.issuenotepad.webapp.Constant;
import org.samearch.issuenotepad.webapp.RequestUtil;
import org.samearch.issuenotepad.webapp.ViewUtil;
import org.samearch.issuenotepad.webapp.XmlUtil;
import spark.Request;
import spark.Response;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.HashMap;
import java.util.List;

/**
 * Контроллер страницы, отображающей список задач.
 *
 * Решаемые задачи:
 * 1. Вывод ранее внесенной информации о задачах
 * 2. Первоначальная обработка файла, загружаемого пользователем
 */
@Named
public class TaskListController {
	private final TaskService taskService;

	private final String SESSION_ERROR_MESSAGE = "errorMessage";

	@Inject
	public TaskListController(TaskService taskService) {
		this.taskService = taskService;
	}

	public Object serveGetIssues(Request request, Response response) {
		HashMap<String, Object> templateContext = new HashMap<>();

		String errorMessage = request.session().attribute(SESSION_ERROR_MESSAGE);
		if (null != errorMessage) {
			request.session().removeAttribute(SESSION_ERROR_MESSAGE);
			templateContext.put(SESSION_ERROR_MESSAGE, errorMessage);
		}
		List<Task> tasks = taskService.getTasks();
		if (null != tasks && tasks.size() > 0)
			templateContext.put("tasks", taskService.getTasks());
		return ViewUtil.render(request, templateContext, Constant.TEMPLATE_ISSUES);
	}

	/**
	 * Логика обработки запроса пользователя, содержащего файл:
	 *
	 * 1. Производится попытка получить данные файла. Предполагается, что файл отправлен через форму, а название поля
	 *    формы, в котором содержится файл, соответствует значению Constant.PAGE_ISSUE_DATA_FILE_FIELD.
	 * 2. Если вместе с формой был отправлен файл, то производится попытка его валидации с помощью схемы, путь к которой
	 *    задан в Constant.PATH_TO_XSD.
	 *    Если валидация файла не удалась, то в сессионной переменной SESSION_ERROR_MESSAGE сохраняется сообщение об ошибке
	 *    и осуществляется переход к п.3.
	 * 2.1. Осуществаляется попытка преобразования данных из файла в объекты, пригодные для обработки с помощью
	 *      IssueService. Объекты передаются сервису.
	 * 3. Пользователь редиректится на страницу отображения данных о задачах.
	 */
	public Object servePostIssues(Request request, Response response) {
		try {
			String requestData = RequestUtil.getStringDataFromPart(request, Constant.PAGE_ISSUE_DATA_FILE_FIELD);
			if (null != requestData) {
				try {
					XmlUtil.validateXml(requestData, Constant.PATH_TO_XSD);
					List<Task> issuesData = XmlUtil.getIssues(requestData);
					TaskService.TaskOperationResult issuesCreationResult = taskService.addTasks(issuesData);
					if (!issuesCreationResult.isOk()) {
						String errorMessage = String.join("\n", issuesCreationResult.getErrors());
						request.session().attribute(SESSION_ERROR_MESSAGE, errorMessage);
					}
				} catch (UnrecognizedPropertyException ignore) {
					/*
					 * Особенности работы Jackson
					 * Исключение генерируется в том случае, если в документе остутствует описание проектов:
					 * <projects></projects>
					 * При этом, если документ будет иметь вид:
					 * <projects/>
					 * то исключение сгенерировано не будет, а документ будет преобразован в пустой список.
					 * Игнорирование исключения допустимо, если документ предварительно прошёл проверку по схеме.
					 */
				}
			}
		} catch (Exception e) {
			request.session().attribute(SESSION_ERROR_MESSAGE, e.getMessage());
			e.printStackTrace();
		}

		return ViewUtil.redirect(response, request.pathInfo());
	}
}
