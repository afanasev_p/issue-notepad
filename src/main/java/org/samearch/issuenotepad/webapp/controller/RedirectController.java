package org.samearch.issuenotepad.webapp.controller;

import spark.Request;
import spark.Response;
import spark.Route;

/**
 * Предоставляет возможность безусловного редиректа пользователя на указанный URL.
 */
public class RedirectController implements Route {
	private final String target;

	public RedirectController(String target) {
		this.target = target;
	}

	@Override
	public Object handle(Request request, Response response) throws Exception {
		response.redirect(target);
		return null;
	}
}
