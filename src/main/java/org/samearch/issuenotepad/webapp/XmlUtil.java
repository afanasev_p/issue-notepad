package org.samearch.issuenotepad.webapp;

import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.samearch.issuenotepad.api.Task;
import org.samearch.issuenotepad.api.Project;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

public class XmlUtil {
	/**
	 * Производит проверку xml-документа на соответствие с указанной схемой.
	 *
	 * @param xmlDocument текстовое представление xml-документа (содержимое xml-файла)
	 * @param schemaPath путь к файлу с описанием схемы документа
	 * @throws SAXException на ошибки загрузки xsd-схемы и ошибки валидации
	 * @throws IOException на ошибки чтения проверяемого документа
	 */
	public static void validateXml(String xmlDocument, String schemaPath) throws SAXException, IOException {
		Source sourceSchemaData = new StreamSource(new BufferedReader(new InputStreamReader(XmlUtil.class.getResourceAsStream(schemaPath))));
		Source sourceXmlData = new StreamSource(new ByteArrayInputStream(xmlDocument.getBytes()));
		SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = schemaFactory.newSchema(sourceSchemaData);
		Validator validator = schema.newValidator();
		validator.validate(sourceXmlData);
	}

	/**
	 * Преобразует xml-документ в набор задач.
	 *
	 * @throws IOException на ошибки парсера. Если документ был предварительно проверен, то исключение может игнорироваться
	 */
	public static List<Task> getIssues(String xmlDocument) throws IOException {
		XmlMapper mapper = new XmlMapper();
		CollectionType projectsCollectionType = mapper.getTypeFactory().constructCollectionType(List.class, Project.class);
		List<Project> projects = mapper.readValue(xmlDocument, projectsCollectionType);
		return projects.stream().map(Project::getTasks).flatMap(List::stream).collect(Collectors.toList());
	}
}
