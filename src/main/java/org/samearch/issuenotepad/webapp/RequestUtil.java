package org.samearch.issuenotepad.webapp;

import spark.Request;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletException;
import javax.servlet.http.Part;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

/**
 * Предоставляет набор утилит для работы с запросом.
 */
public class RequestUtil {
	/**
	 * Возвращает содержимое текстового файла, который был отправлен через форму.
	 *
	 * @param request пользовательский запрос
	 * @param partName имя поля, в котором должен "находится" файл (значение атрибута "name" тега <input type="file" ...>)
	 * @return строковое представление файла
	 */
	public static String getStringDataFromPart(Request request, String partName) throws IOException, ServletException {
		String stringData = null;
		request.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement("/temp"));
		Part stringDataPart = request.raw().getPart(partName);
		if (null != stringDataPart) {
			InputStream is = stringDataPart.getInputStream();
			stringData = new BufferedReader(new InputStreamReader(is)).lines().collect(Collectors.joining("\n"));
			is.close();
		}
		return stringData;
	}
}
