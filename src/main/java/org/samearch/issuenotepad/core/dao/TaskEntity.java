package org.samearch.issuenotepad.core.dao;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.samearch.issuenotepad.api.Project;
import org.samearch.issuenotepad.api.Task;

@DatabaseTable(tableName = "task")
public class TaskEntity {
	@DatabaseField(id = true)         public String key;
	@DatabaseField(canBeNull = false) public String projectKey;
	@DatabaseField(canBeNull = false) public String summary;
	@DatabaseField                    public String description;

	public static TaskEntity fromTask(Task task) {
		if (null == task)
			return null;
		TaskEntity taskEntity = new TaskEntity();
		taskEntity.projectKey = task.project.key;
		taskEntity.key = task.key;
		taskEntity.summary = task.summary;
		taskEntity.description = task.description;
		return taskEntity;
	}

	public Task toTask() {
		Task task = new Task();
		task.project = new Project();
		task.project.key = projectKey;
		task.key = key;
		task.summary = summary;
		task.description = description;
		return task;
	}
}
