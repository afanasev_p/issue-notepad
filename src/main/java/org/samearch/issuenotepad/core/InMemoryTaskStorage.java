package org.samearch.issuenotepad.core;

import org.samearch.issuenotepad.api.Task;
import org.samearch.issuenotepad.api.TaskStorage;

import javax.inject.Named;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//@Named
public class InMemoryTaskStorage implements TaskStorage {
	private final HashMap<String, Task> issues = new HashMap<>();

	public Task create(Task task) throws SQLIntegrityConstraintViolationException {
		if (null == task)
			throw new NullPointerException("Issue and issue key can not be null");
		if (null == task.key)
			throw new SQLIntegrityConstraintViolationException("Issue key can not be null");
		if (issues.containsKey(task.key))
			throw new SQLIntegrityConstraintViolationException("Issue " + task.key + " already exists");
		issues.put(task.key, task);
		return task;
	}

	public void delete(Task task) {
		if (null != task)
			delete(task.key);
	}

	public void delete(String issueKey) {
		if (null != issueKey)
			issues.remove(issueKey);
	}

	public Task get(String issueKey) {
		if (null == issueKey)
			return null;
		return issues.get(issueKey);
	}

	@Override
	public List<Task> getAll() {
		return new ArrayList<>(issues.values());
	}

	public Task update(Task task) throws IssueNotFoundException {
		if (null == task)
			throw new IssueNotFoundException(null);
		if (null == task.key || !issues.containsKey(task.key))
			throw new IssueNotFoundException(task.key);

		return task;
	}
}
