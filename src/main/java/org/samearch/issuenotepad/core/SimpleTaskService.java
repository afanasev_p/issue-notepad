package org.samearch.issuenotepad.core;

import org.samearch.issuenotepad.api.Task;
import org.samearch.issuenotepad.api.TaskService;
import org.samearch.issuenotepad.api.TaskStorage;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Простая реализация сервиса управления информацией о задача.
 *
 * В текущей реализации никаких дополнительных действий (проверок, etc) не производится.
 */
@Named
public class SimpleTaskService implements TaskService {
	private final TaskStorage taskStorage;

	@Inject
	public SimpleTaskService(TaskStorage taskStorage) {
		this.taskStorage = taskStorage;
	}

	public TaskOperationResult addTask(Task task) {
		return addIssue(task, new TaskOperationResult());
	}

	public TaskOperationResult addTasks(List<Task> tasks) {
		TaskOperationResult result = new TaskOperationResult();
		if (null != tasks) {
			tasks.forEach(issue -> addIssue(issue, result));
		}
		return result;
	}

	public List<Task> getTasks() {
		return taskStorage.getAll();
	}

	@Override
	public Task getTask(String taskKey) {
		return taskStorage.get(taskKey);
	}

	@Override
	public void deleteTask(String taskKey) {
		taskStorage.delete(taskKey);
	}

	private TaskOperationResult addIssue(Task task, TaskOperationResult taskOperationResult) {
		if (null == task) {
			taskOperationResult.addError("Issue object can not be null");
		} else {
			try {
				taskOperationResult.addIssue(taskStorage.create(task));
			} catch (Exception e) {
				taskOperationResult.addError(e.getMessage());
			}
		}
		return taskOperationResult;
	}
}
