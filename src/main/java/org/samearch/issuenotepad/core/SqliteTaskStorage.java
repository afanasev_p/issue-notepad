package org.samearch.issuenotepad.core;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.table.TableUtils;
import org.samearch.issuenotepad.api.Task;
import org.samearch.issuenotepad.api.TaskStorage;
import org.samearch.issuenotepad.core.dao.TaskEntity;

import javax.annotation.PreDestroy;
import javax.inject.Named;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Хранилище информации о задачах в sqlite-базе.
 */
@Named
public class SqliteTaskStorage implements TaskStorage {
	// TODO: переписать реализацию класса таким образом, чтобы строку подключения к БД можно было передавать параметром
	private static final String CONNECTION_STRING = "jdbc:sqlite:issuesNotepad.sqlite";

	private final JdbcConnectionSource connectionSource;

	/**
	 * Подготавливает БД перед использованием.
	 *
	 * @throws RuntimeException при ошибках подключения к СУБД
	 */
	public SqliteTaskStorage() throws RuntimeException {
		try {
			connectionSource = new JdbcConnectionSource(CONNECTION_STRING);
			TableUtils.createTableIfNotExists(connectionSource, TaskEntity.class);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@PreDestroy
	public void preDestroy() {
		try {
			connectionSource.close();
		} catch (IOException ignore) {}
	}

	private Dao<TaskEntity, String> getTaskDao() {
		try {
			return DaoManager.createDao(connectionSource, TaskEntity.class);
		} catch (SQLException e) {
			throw new RuntimeException(e.getCause());
		}
	}

	@Override
	public Task create(Task task) throws SQLIntegrityConstraintViolationException {
		Dao<TaskEntity, String> taskDao = getTaskDao();
		TaskEntity createdTaskEntity = TaskEntity.fromTask(task);
		try {
			taskDao.create(createdTaskEntity);
			return taskDao.queryForId(task.key).toTask();
		} catch (SQLIntegrityConstraintViolationException e) {
			throw e;
		} catch (SQLException e) {
			throw new RuntimeException(e.getCause());
		}
	}

	@Override
	public void delete(Task task) {
		if (null != task)
			delete(task.key);
	}

	@Override
	public void delete(String taskKey) {
		if (null == taskKey || "".equals(taskKey))
			return;
		Dao<TaskEntity, String> taskDao = getTaskDao();
		try {
			TaskEntity taskEntity = taskDao.queryForId(taskKey);
			if (null != taskEntity) {
				taskDao.delete(taskEntity);
			}
		} catch (Exception ignore) {}
	}

	@Override
	public Task get(String taskKey) {
		try {
			TaskEntity taskEntity = getTaskDao().queryForId(taskKey);
			return (null != taskEntity)
					? taskEntity.toTask()
					: null;
		} catch (SQLException e) {
			throw new RuntimeException(e.getCause());
		}
	}

	@Override
	public List<Task> getAll() {
		return StreamSupport.stream(Spliterators.spliteratorUnknownSize(getTaskDao().iterator(), 0), false)
				.map(TaskEntity::toTask)
				.collect(Collectors.toList());
	}

	@Override
	public Task update(Task task) {
		if (null == task || null == task.key || "".equals(task.key)) {
			throw new RuntimeException("can't update undefined task");
		}
		Dao<TaskEntity, String> taskDao = getTaskDao();
		try {
			taskDao.update(TaskEntity.fromTask(task));
			return taskDao.queryForId(task.key).toTask();
		} catch (SQLException e) {
			throw new RuntimeException(e.getCause());
		}
	}
}
