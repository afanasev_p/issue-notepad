package org.samearch.issuenotepad.api;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Task {
	@XmlElement @JsonBackReference public Project project;
	@XmlElement	public String key;
	@XmlElement	public String summary;
	@XmlElement	public String description;

	public Project getProject() { return project; }
	public String getKey() { return key; }
	public String getSummary() { return summary; }
	public String getDescription() { return description; }
}
