package org.samearch.issuenotepad.api;

import java.util.ArrayList;
import java.util.List;

/**
 * Сервис управления задачами.
 *
 * Цель выделения сервиса для работы с задачами - собрать высокоуровневые функции и логику различных проверок в одном месте.
 */
public interface TaskService {
	TaskOperationResult addTask(Task task);
	TaskOperationResult addTasks(List<Task> tasks);
	List<Task> getTasks();
	Task getTask(String taskKey);
	void deleteTask(String taskKey);

	/**
	 * Используется для индикации статуса завершения операций с issue
	 */
	final class TaskOperationResult {
		private final List<Task> tasks = new ArrayList<>();
		private final List<String> errors= new ArrayList<>();

		public boolean isOk() {
			return errors.size() == 0;
		}

		public List<Task> getTasks() {
			return tasks;
		}

		public List<String> getErrors() {
			return errors;
		}

		public TaskOperationResult addIssue(Task task) {
			tasks.add(task);
			return this;
		}

		public TaskOperationResult addError(String errorMessage) {
			errors.add(errorMessage);
			return this;
		}
	}
}
