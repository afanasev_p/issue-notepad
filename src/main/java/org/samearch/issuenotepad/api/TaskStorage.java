package org.samearch.issuenotepad.api;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

/**
 * Сервис сохранения данных о задачах.
 *
 * Выделяет низкоуровневые CRUD-операции в отдельный слой. Никаких проверок этот слой не производит.
 *
 * Для упрощения реализации в качестве уникального ключа использует ключ задачи.
 */
public interface TaskStorage {
	Task create(Task task) throws SQLIntegrityConstraintViolationException;
	void delete(Task task);
	void delete(String issueKey);
	Task get(String issueKey);
	List<Task> getAll();
	Task update(Task task) throws IssueNotFoundException;

	final class IssueNotFoundException extends Exception {
		private final String issueKey;

		public IssueNotFoundException(String issueKey) {
			super("Issue " + issueKey + " not found");
			this.issueKey = issueKey;
		}

		public String getIssueKey() {
			return issueKey;
		}
	}
}
