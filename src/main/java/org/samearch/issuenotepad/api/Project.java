package org.samearch.issuenotepad.api;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class Project {
	@XmlElement public String key;
	@XmlElement	public String summary;
	@XmlElement	@JsonManagedReference public List<Task> tasks;

	public String getKey() { return key; }
	public String getSummary() { return summary; }
	public List<Task> getTasks() { return tasks; }
}
